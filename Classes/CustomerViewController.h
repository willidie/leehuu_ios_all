//
//  CustomerViewController.h
//  linphone
//
//  Created by zhangxm on 13-12-18.
//
//

#import <UIKit/UIKit.h>
#import "UICompositeViewController.h"
@class UIShowWebViewController;
@interface CustomerViewController : UIViewController
{
    UIShowWebViewController *showWebViewController;
      UIButton *btguestserve;
      UIButton *btcommonMatter;
      UIButton *btchineseSip;
      UIButton *btJapaneseSip;
      UIButton *btMail;
    UILabel *zhongwen;
    
     UILabel *riwen;
    
    UILabel *labelTel;
    
    UILabel *labelSip;
}
@property(retain,nonatomic)IBOutlet UIButton *btguestserve;
@property(retain,nonatomic)IBOutlet UIButton *btcommonMatter;
@property(retain,nonatomic)IBOutlet UIButton *btchineseSip;
@property(retain,nonatomic)IBOutlet UIButton *btJapaneseSip;
@property(retain,nonatomic)IBOutlet UIButton *btMail;
@property(retain,nonatomic)IBOutlet UILabel *zhongwen;
@property(retain,nonatomic)IBOutlet  UILabel *labelTel;
@property(retain,nonatomic)IBOutlet  UILabel *labelSip;
@property(retain,nonatomic)IBOutlet UILabel *riwen;
@property (nonatomic, strong) UIWebView* webView;
+ (UICompositeViewDescription *)compositeViewDescription;
@property(nonatomic,retain) UIShowWebViewController *showWebViewController;
-(IBAction)btguestserve:(id)sender;
-(IBAction)btcommonMatter:(id)sender;
-(IBAction)btchineseCall:(id)sender;
-(IBAction)btJapanesecall:(id)sender;
-(IBAction)btchineseSip:(id)sender;
-(IBAction)btJapaneseSip:(id)sender;
-(IBAction)btMail:(id)sender;
@end
