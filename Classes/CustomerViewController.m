//
//  CustomerViewController.m
//  linphone
//
//  Created by zhangxm on 13-12-18.
//
//

#import "CustomerViewController.h"
#import "LinphoneManager.h"
#import "PhoneMainView.h"

#import <Foundation/Foundation.h>
#import "UIShowWebViewController.h"
@interface CustomerViewController ()<UIWebViewDelegate>

@end

@implementation CustomerViewController
@synthesize showWebViewController;
@synthesize btguestserve;
@synthesize btcommonMatter;
@synthesize btchineseSip;
@synthesize btJapaneseSip;
@synthesize btMail;
@synthesize zhongwen;
@synthesize riwen;
@synthesize labelTel;
@synthesize labelSip;
@synthesize webView = _webView;

static UICompositeViewDescription *compositeDescription = nil;

+ (UICompositeViewDescription *)compositeViewDescription {
    if(compositeDescription == nil) {
        compositeDescription = [[UICompositeViewDescription alloc] init:@"Customer"
                                                                content:@"CustomerViewController"
                                                               stateBar:@"UIStateBar"
                                                        stateBarEnabled:true
                                                                 tabBar:@"UIMainBar"
                                                          tabBarEnabled:true
                                                             fullscreen:false
                                                          landscapeMode:[LinphoneManager runningOnIpad]
                                                           portraitMode:true];
    }
    return compositeDescription;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //self.tabBarItem.title = @"微门户";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self.btguestserve  setTitle:NSLocalizedString(@"guestserve", nil) forState:normal];
    [self.btcommonMatter  setTitle:NSLocalizedString(@"commonMatter", nil) forState:normal];
	
    [self.btchineseSip  setTitle:NSLocalizedString(@"chineseSip", nil) forState:normal];
	
    [self.btJapaneseSip  setTitle:NSLocalizedString(@"JapaneseSip", nil) forState:normal];
	
    [self.btMail  setTitle:NSLocalizedString(@"mailconsult", nil) forState:normal];
	
    [self.zhongwen setText:NSLocalizedString(@"chineseSip", nil)];
    
    [self.riwen setText:NSLocalizedString(@"JapaneseSip", nil)];
    NSString *string=NSLocalizedString(@"labelTel", nil);
    NSString *string2=NSLocalizedString(@"labelSip", nil);
    
     [self.labelTel setText:string];
     [self.labelSip setText:string2];
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 50, 320, self.view.frame.size.height-70-50-10)];
    self.webView.backgroundColor = [UIColor blackColor];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://182.92.157.215/index.php?g=Wap&m=Index&a=index&token=cohehn1405910490"]]];
    
}
-(void)dealloc
{
    self.btguestserve=nil;
     self.btcommonMatter=nil;
     self.btchineseSip=nil;
     self.btJapaneseSip=nil;
     self.btMail=nil;
    self.zhongwen=nil;
    self.riwen=nil;
    self.labelTel=nil;
    self.labelSip=nil;
    [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}-(IBAction)btguestserve:(id)sender
{
    //[self.btcommonMatter setTintColor:[UIColor whiteColor]];
    //[self.btcommonMatter setTintColor:[UIColor blackColor]];
    [self.btcommonMatter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btguestserve setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://182.92.157.215/index.php?g=Wap&m=Index&a=index&token=d35753df4969f95"]]];
    
   // [UIApplication sharedApplication]openURL:<#(NSURL *)#>
}

-(IBAction)btcommonMatter:(id)sender
{
    [self.btcommonMatter setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btguestserve setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://182.92.157.215/index.php?g=Wap&m=Store&a=cats&token=d35753df4969f95"]]];
   // self.showWebViewController=[[[UIShowWebViewController alloc]initWithNibName:@"UIShowWebViewController" bundle:nil]autorelease];
    //[[PhoneMainView instance] changeCurrentView:[UIShowWebViewController compositeViewDescription]];

    
     ///   [self.showWebViewController loadWebPageWithString:@"http://www.ncccall.com/faq/ios"];
}
- (IBAction)btYE:(UIButton *)sender {
//    NSString* username = [self stringForKey:@"username_preference"];
//    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:@"http://ixincallapi.ixincall.com/vos/api/balance2?a=%@&b=%@",_word,_name]];
//    NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30.0];
//    
//    NSURLConnection *connent =[[NSURLConnection alloc]initWithRequest:request delegate:self];
//    [connent start];
    NSString *lTitle=@"余额";
    NSString *lMessage=@"100元";
    UIAlertView* error = [[UIAlertView alloc] initWithTitle:lTitle
                                                    message:lMessage
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"确认",nil)
                                          otherButtonTitles:nil];
    [error show];
    [error release];
}

-(IBAction)btchineseCall:(id)sender
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"tel://18939892185"]];
}
-(IBAction)btJapanesecall:(id)sender
{
   
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"tel://08046548168"]];
}
-(IBAction)btchineseSip:(id)sender
{
    [[LinphoneManager instance]call:@"10008" displayName:@"10008" transfer:NO];
}

-(IBAction)btJapaneseSip:(id)sender
{
    [[LinphoneManager instance]call:@"10000" displayName:@"10000" transfer:NO];
}
-(IBAction)btMail:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://87293919@qq.com"]];
  
}

@end
