//
//  UIShowWebViewController.h
//  GovPro
//
//  Created by zhangxm on 12-11-16.
//
//

#import <UIKit/UIKit.h>
#import "UICompositeViewController.h"
@interface UIShowWebViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
     UIActivityIndicatorView *activityIndicatorView;
    
}
@property(retain,nonatomic) UIWebView *webView;
+ (UICompositeViewDescription *)compositeViewDescription;
- (void)loadWebPageWithString:(NSString*)urlString;
@end
