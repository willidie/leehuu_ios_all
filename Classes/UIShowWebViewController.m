//
//  UIShowWebViewController.m
//  GovPro
//
//  Created by zhangxm on 12-11-16.
//
//

#import "UIShowWebViewController.h"
#import "PhoneMainView.h"
#import "ImageExtras.h"
@interface UIShowWebViewController ()

@end
static UICompositeViewDescription *compositeDescription = nil;
@implementation UIShowWebViewController
@synthesize webView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


+ (UICompositeViewDescription *)compositeViewDescription {
    if(compositeDescription == nil) {
        compositeDescription = [[UICompositeViewDescription alloc] init:@"UIShowWeb"
                                                                content:@"UIShowWebViewController"
                                                               stateBar:@"UIStateBar"
                                                        stateBarEnabled:false
                                                                 tabBar:@"UIMainBar"
                                                          tabBarEnabled: false
                                                             fullscreen:true
                                                          landscapeMode:[LinphoneManager runningOnIpad]
                                                           portraitMode:true];
    }
    return compositeDescription;
}

- (void)loadWebPageWithString:(NSString*)urlString
{
    
    NSURL *url =[NSURL URLWithString:urlString];
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
}
- (IBAction)onbtbackClick:(id)sender {
    [[PhoneMainView instance] changeCurrentView:[CustomerViewController compositeViewDescription] push:TRUE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    webView.scalesPageToFit =YES;
      webView.delegate =self;
    activityIndicatorView = [[UIActivityIndicatorView alloc]
                             initWithFrame : CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)] ;
    [activityIndicatorView setCenter: self.view.center] ;
    [activityIndicatorView setActivityIndicatorViewStyle: UIActivityIndicatorViewStyleWhite] ;
    [self.view addSubview : activityIndicatorView] ;
    
    UIImage* img=[UIImage imageNamed:@"返回按钮"];
    CGSize csize;
    csize.width=40;
    csize.height=40;
    [img imageByScalingToSize:csize];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
 
    
    btn.frame =CGRectMake(0, 0, 40, 40);
    
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    
    [btn addTarget: self action: @selector(onbtbackClick:) forControlEvents: UIControlEventTouchUpInside];
    
    UIBarButtonItem* item=[[[UIBarButtonItem alloc]initWithCustomView:btn]autorelease];
    
    
    UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    //创建一个导航栏集合
    UINavigationItem *navigationItem = [[[UINavigationItem alloc] initWithTitle:nil]autorelease];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    titleLabel.font = [UIFont boldSystemFontOfSize:25];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    // titleLabel.textAlignment = UITextAlignmentLeft;
    titleLabel.text = @"         常见问题";
    navigationItem.titleView = titleLabel;
    
    //把导航栏集合添加入导航栏中，设置动画关闭
    [navigationBar pushNavigationItem:navigationItem animated:NO];
    
    
    [navigationItem setLeftBarButtonItem:item];
    
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (version >= 7.0) {
        UIImage *backgroundImage = [UIImage imageNamed:@"导航条"];
        
        if ([navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]){
            [navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
            
        }
    }
    else
    {
        CGSize csizeimage;
        csizeimage.width=320;
        csizeimage.height=64;
        navigationBar.tintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"导航条"]];
        
    }
    
    //把导航栏添加到视图中
    [self.view addSubview:navigationBar];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [activityIndicatorView startAnimating] ;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    UIAlertView *alterview = [[UIAlertView alloc] initWithTitle:@"" message:[error localizedDescription]  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alterview show];
    [alterview release];
}
-(void)viewDidUnload
{
    self.webView=nil;
    [super viewDidUnload];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
